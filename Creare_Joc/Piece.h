#pragma once
#include<iostream>
class Piece
{
	// o piesa o sa aiba urmatoarele enum-uri: body, color, height, shape
public: 
	enum class Body
	{
		Full,
	    Hollow
	};

	enum class Color
	{
		Dark,
		Light
	};

	enum class Height
	{
		Short,
		Tall
	};

	enum class Shape
	{
		Round,
		Square
	};

private: 
	Body m_body;
	Color m_color;
	Height m_height;
	Shape m_shape;
public:
	Piece(Body body, Color color, Height height, Shape shape);
	Body getBody() const;
	Color getColor() const;
	Height getHeight() const;
	Shape getShape() const;

	friend std::ostream& operator << (std::ostream& out, Piece& piece);

};

