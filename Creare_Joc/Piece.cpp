#include "Piece.h"

Piece::Piece(Body body, Color color, Height height, Shape shape):
	m_body(body),
	m_color(color),
	m_height(height),
	m_shape(shape)
{
}

Piece::Body Piece::getBody() const
{
	return m_body;
}

Piece::Color Piece::getColor() const
{
	return m_color;
}

Piece::Height Piece::getHeight() const
{
	return m_height;
}

Piece::Shape Piece::getShape() const
{
	return m_shape;
}

std::ostream& operator<<(std::ostream& out, Piece& piece)
{
	if (piece.m_body == Piece::Body::Full) out << "Full ";
	else out << "Hollow ";
	if (piece.m_color == Piece::Color::Dark) out << "Dark ";
	else out << "Light ";
	if (piece.m_height == Piece::Height::Short) out << "Short ";
	else out << "Tall ";
	if (piece.m_shape == Piece::Shape::Round) out << "Round ";
	else out << "Square ";
	//out << static_cast<int>(piece.m_body) << " " <<static_cast<int>( piece.m_color) << " "<<static_cast<int>(piece.m_height) << " " << static_cast<int>(piece.m_shape);

	return out;
}
